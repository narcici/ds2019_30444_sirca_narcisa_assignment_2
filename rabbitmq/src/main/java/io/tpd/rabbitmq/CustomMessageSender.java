package io.tpd.rabbitmq;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import java.util.Random;


@Service
public class CustomMessageSender {

    private static final Logger log = LoggerFactory.getLogger(CustomMessageSender.class);

    private final RabbitTemplate rabbitTemplate;

    private int currentActivityIt = 0;

    private ArrayList<ActivityRabbit> activities = new ArrayList<>();

    public CustomMessageSender(final RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(fixedDelay = 1000L)
    public void sendMessage() {

        if (activities.isEmpty()) {
            initActivityList();
        }

        ActivityRabbit currentActivityRabbit = activities.get(this.currentActivityIt);
        log.info("Sending message..." + currentActivityRabbit.toString()); //se punee
        this.currentActivityIt++;
        rabbitTemplate.convertAndSend(RabbitmqApplication.EXCHANGE_NAME, RabbitmqApplication.ROUTING_KEY, currentActivityRabbit);

    }

    private void initActivityList() {

        //full path bc why not?!?
        //String fileName = "D:\\AC\\Anul_4\\DISTRIBUTED_SYSTEMS\\narci\\ds2019_30444_sirca_narcisa_assignment_1\\rabbitmq\\src\\main\\resources\\activities.txt";
        String fileName = "D:\\Cti4sem1\\DS2019_30444_Sirca_Narcisa_Assignment_1\\ds2019_30444_sirca_narcisa_assignment_1\\rabbitmq\\src\\main\\resources\\activities.txt";
        List<String> content = new ArrayList<String>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //Simple Strings list
        // read file into stream, try-with-resources
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            // here I put the input in a collection of String elem called <<<<<<< content
            content = stream.collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        content.stream().forEach(x -> {

            String[] temporal = x.split("[	]+");
           /// int id = new Random().nextInt(3); //good for more patients
            try {
                Date start = formatter.parse(temporal[0]);
                Date end = formatter.parse(temporal[1]);
                String activity = temporal[2];

                activities.add(new ActivityRabbit(1, activity, start, end));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
    }
}