package com.example.springdemo.controller;

import com.example.springdemo.dto.*;
import com.example.springdemo.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    // get all doctors
    @GetMapping()
    public List<DoctorDTO> findAllDoctors(){
        return doctorService.findAllDoctors();
    }

    // get doctor by id and all its patients DoctorDTO too
    //find all patients of this doctor
    @GetMapping(value = "/{id}")
    public DoctorWithPatientsDTO findById(@PathVariable("id") Integer id){
        return doctorService.findDoctorById(id);
    }

}
