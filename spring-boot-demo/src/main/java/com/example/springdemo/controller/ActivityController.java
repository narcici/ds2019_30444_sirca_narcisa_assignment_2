package com.example.springdemo.controller;

import com.example.springdemo.event.BaseEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ActivityController {

    private final SimpMessagingTemplate messagingTemplate;

    @EventListener(BaseEvent.class)
    public void handleEvent(BaseEvent event) {
        log.info("Activity primita {}.", event); ///NU VAD
        messagingTemplate.convertAndSend("/activity" +
                "/events", event);
    }
}
