package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.ActivityDTO;
import com.example.springdemo.entities.Activity;

public class ActivityBuilder {

    public ActivityBuilder() {
    }

    public static ActivityDTO generateDTOFromEntity(Activity activity){
        return new ActivityDTO(
                activity.getActivityId(),
                activity.getId(),
                activity.getActivity(),
                activity.getStart(),
                activity.getEnd());
    }

    public static Activity generateEntityFromDTO(ActivityDTO activityDTO){
        return new Activity(
                activityDTO.getActivityId(),
                activityDTO.getId(),
                activityDTO.getActivity(),
                activityDTO.getStart(),
                activityDTO.getEnd());
    }
}
