package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.entities.Doctor;

public class DoctorBuilder {

    public DoctorBuilder() {
    }

    public static DoctorDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorDTO(
                doctor.getUser(),
                doctor.getDoctorId(),
                doctor.getName() );
    }

    public static Doctor generateEntityFromDTO(DoctorDTO doctorDTO){
        return new Doctor(
                doctorDTO.getUser(),
                doctorDTO.getDoctorId(),
                doctorDTO.getName());
    }
}
