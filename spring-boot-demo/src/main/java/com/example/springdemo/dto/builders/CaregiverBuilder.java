package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.entities.Caregiver;

public class CaregiverBuilder {

    /*
    private Integer user; //inherited userid
    private Integer caregiverId;
    private String name;
    private  String birthdate;
    private String address;
    private Character gender;
     */
    public CaregiverBuilder() {
    }

    public static CaregiverDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverDTO(
                caregiver.getUser(),
                caregiver.getCaregiverId(),
                caregiver.getName(),
                caregiver.getBirthdate(),
                caregiver.getAddress(),
                caregiver.getGender());
    }

    public static Caregiver generateEntityFromDTO(CaregiverDTO caregiverDTO){
        return new Caregiver(
                caregiverDTO.getUser(),
                caregiverDTO.getCaregiverId(),
                caregiverDTO.getName(),
                caregiverDTO.getBirthdate(),
                caregiverDTO.getAddress(),
                caregiverDTO.getGender());
    }
}