package com.example.springdemo.seed;

import com.example.springdemo.entities.*;
import com.example.springdemo.repositories.*;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;
/*
aici clasa pe care o rulez ca sa am db initializat
 */


//@Component
//@Order(Ordered.HIGHEST_PRECEDENCE)
@RequiredArgsConstructor
public class ApplicationSeed implements CommandLineRunner {

    private final UserRepository userRepository;
    private final CaregiverRepository caregiverRepository;
    private final DoctorRepository doctorRepository;
    private final PatientRepository patientRepository;
    private final MedicationRepository medicationRepository;
    private final MedicationPlanRepository medicationPlanRepository;
    private final IntakeIntervalRepository intakeIntervalRepository;

    @Override
    public void run(String... args) throws Exception {
        User user = userRepository.save(new User(1,"doc","doc", (byte)0));
        System.out.println(user.getUsername() + " " + user.getId() + " " + user.getClass());

        // 1 doctor
        Doctor doctor = new  Doctor(user, 1, "Doctorul 1");
        doctor = doctorRepository.save(doctor);
        System.out.println(doctor.getDoctorId() + " "+ doctor.getClass());

        //2 caregivers
        User user2 = new User(2, "care", "care", (byte)2);
        userRepository.save(user2);
        Caregiver caregiver = caregiverRepository.save(new Caregiver(user2,1,"Caregiver",
                "29.05.1997","Plopilor",'F'));
        System.out.println(caregiver.getName() + " " + caregiver.getCaregiverId() + caregiver.getClass());


        User user3 = new User(3, "boom", "boom", (byte)2);
        userRepository.save(user3);
        Caregiver caregiver2 = caregiverRepository.save(new Caregiver(user3,2,"Alt caregiver","16.04.1997","Plopilor",'F'));
        System.out.println(caregiver2.getName() + " " + caregiver2.getCaregiverId() + caregiver2.getClass());

        System.out.println();

        //caregiverRepository.deleteById(caregiver.getId());
        List<Caregiver> caregivers = caregiverRepository.findAll();
        for (Caregiver iterator : caregivers) {
            System.out.println(iterator.getName() + " " + iterator.getCaregiverId() + " " + iterator.getBirthdate());
        }
        System.out.println();

        //4 patients         ana dani ela tudor
        User user4 = new User(4, "ana", "ana", (byte)1);
        userRepository.save(user4);
       Patient patient = new Patient(user4, 1, "Ana A", "20.02.2000",
               'F', "Motilor 34", "nu are");
       patient.setDoctor(doctor);
       patient.setCaregiver(caregiver);
       patient = patientRepository.save(patient);
       System.out.println(patient.getPatientid()+ " " + patient.getName());

        User user5 = new User(5, "dani", "dani", (byte)1);
        userRepository.save(user5);
        Patient patient2 = new Patient(user5, 2, "Dani A", "20.09.2000",
                'M', "Motilor 34", "nu are");
        patient2.setDoctor(doctor);
        patient2.setCaregiver(caregiver);
        patient2 = patientRepository.save(patient2);
        System.out.println(patient2.getPatientid()+ " " + patient2.getName());

        User user6 = new User(6, "ela", "ela", (byte)1);
        userRepository.save(user6);
        Patient patient3 = new Patient(user6, 3, "Ela", "11.11.2000",
                'F', "Iris 34", "nu are");
        patient3.setDoctor(doctor);
        patient3.setCaregiver(caregiver2);
        patient3 = patientRepository.save(patient3);
        System.out.println(patient3.getPatientid()+ " " + patient3.getName());

        User user7 = new User(7, "tudor", "tudor", (byte)1);
        userRepository.save(user7);
        Patient patient4 = new Patient(user7, 4, "Tudor T", "20.02.2000",
                'M', "Ceva 34", "nu are");
        patient4.setDoctor(doctor);
        patient4.setCaregiver(caregiver2);
        patient4 = patientRepository.save(patient4);
        System.out.println(patient4.getPatientid()+ " " + patient4.getName());

    // 2 medications
        Medication medication1 = new Medication(1, "Crestor","200", "headache, depression, indigestion");
        medication1 = medicationRepository.save(medication1);

        Medication medication2 = new Medication(2, "Paracetamol","200", "dizziness");
        medication2 = medicationRepository.save(medication2);

        //3 medication plans
        MedicationPlan medicationPlan1= new MedicationPlan(1,
                new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.now().toString()),
                new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2019, Month.DECEMBER, 3).toString()),
                patient);
        medicationPlan1 = medicationPlanRepository.save(medicationPlan1);


        MedicationPlan medicationPlan2= new MedicationPlan(2,
                new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.now().toString()),
                new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2019, Month.NOVEMBER, 15).toString()),
                patient2);
        medicationPlan2 = medicationPlanRepository.save(medicationPlan2);

        MedicationPlan medicationPlan3= new MedicationPlan(3,
                new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.now().toString()),
                new SimpleDateFormat("yyyy-MM-dd").parse(LocalDate.of(2020, Month.APRIL, 15).toString()),
                patient3);
        medicationPlan3 = medicationPlanRepository.save(medicationPlan3);

        /*
            public IntakeInterval(Integer intakeIntervalId, MedicationPlan medicationPlan, Medication medication) {

         */
        IntakeInterval intakeInterval = new IntakeInterval(1, medicationPlan1, medication1);
        intakeInterval.setMorning((byte)1);
        intakeInterval.setEvening((byte)1);
        intakeInterval.setNoon((byte)3);
        intakeInterval = intakeIntervalRepository.save(intakeInterval);

        IntakeInterval intakeInterval1 = new IntakeInterval(2, medicationPlan1, medication2);
        intakeInterval1.setMorning((byte)1);
        intakeInterval1.setEvening((byte)1);
        intakeInterval1.setNoon((byte)3);
        intakeInterval1 = intakeIntervalRepository.save(intakeInterval1);

        IntakeInterval intakeInterval2 = new IntakeInterval(3, medicationPlan2, medication1);
        intakeInterval2.setMorning((byte)2);
        intakeInterval2.setEvening((byte)2);
        intakeInterval2.setNoon((byte)2);
        intakeInterval2 = intakeIntervalRepository.save(intakeInterval2);

        IntakeInterval intakeInterval3 = new IntakeInterval(4, medicationPlan3, medication2);
        intakeInterval3.setMorning((byte)1);
        intakeInterval3.setEvening((byte)1);
        intakeInterval3.setNoon((byte)1);
        intakeInterval3 = intakeIntervalRepository.save(intakeInterval3);

    }
}
