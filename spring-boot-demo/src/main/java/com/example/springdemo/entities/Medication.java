package com.example.springdemo.entities;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication")
public class Medication {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "medication_id", unique = true, nullable = false)
    private Integer medication_id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "dosage", length = 10)
    private String dosage;

    @Column(name = "sideeffects", length = 100)
    private String sideeffects;

    @OneToMany(mappedBy="medication", cascade = CascadeType.ALL,orphanRemoval = true)
    private List<IntakeInterval> intakeIntervalList = new ArrayList<IntakeInterval>();

    public Medication() {}

    public Medication(Integer id, String name, String dosage, String sideeffects){
        this.medication_id = id;
        this.name = name;
        this.dosage = dosage;
        this.sideeffects = sideeffects;
    }
    public Integer getId() { return medication_id; }

    public void setId(Integer id) {this.medication_id = id; }

    public String getName() { return name; }

    public void setName(String name) {  this.name = name;  }

    public String getDosage() { return dosage; }

    public void setDosage(String dosage) { this.dosage = dosage; }

    public String getSideeffects() {  return sideeffects; }

    public void setSideeffects(String sideeffects) { this.sideeffects = sideeffects; }

    public List<IntakeInterval> getIntakeIntervalList() {
        return intakeIntervalList;
    }

    public void setIntakeIntervalList(List<IntakeInterval> intakeIntervalList) {
        this.intakeIntervalList = intakeIntervalList;
    }
}