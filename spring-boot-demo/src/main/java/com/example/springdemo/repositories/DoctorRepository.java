package com.example.springdemo.repositories;

import com.example.springdemo.entities.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Integer> {

    @Query(value = "SELECT d " +
            "FROM Doctor d " +
            "INNER JOIN FETCH d.patients i"
    )
    List<Doctor> getAllFetch();
}
