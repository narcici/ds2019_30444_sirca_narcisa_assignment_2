import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import MedicationForm from "./medication-form";
import UpdateMedicationForm from "./update-medication-form";


import * as API_MEDICATION from "./api/medication-api"

class Medications extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null,
        };

        this.columns = [
            {
                Header: 'Medication',
                accessor: 'name',
            },
            {
                Header: "Dosage",
                accessor: "dosage",
            },
            {
                Header: "Side Effects",
                accessor: "sideeffects",
            },
            {
                Header: 'Update',
                Cell: row => (
                    <div>
                        <button onClick={() => {
                            this.handleUpdate(this.tableData, row.index)
                        }}>Update
                        </button>
                    </div>
                )
            },
            {
                Header: 'Delete',
                Cell: row => (
                    <div>
                        <button onClick={() => {
                            this.handleDelete(this.tableData, row.index)
                        }}>Delete
                        </button>
                    </div>
                )
            }

        ];

        this.tableData = [];

        this.handleUpdate = this.handleUpdate.bind(this);
        this.doTheUpdate = this.doTheUpdate.bind(this);

    }

    handleDelete(table, index) {
        //console.log("delete " + index.toString());
        let deletedMedication = table[index];
        API_MEDICATION.deleteMedication(deletedMedication, () => console.log("m deleted"));
        this.refresh();
    }

    handleUpdate(table, index) {
       // console.log("tabel[index] : ", table[index]);

        this.setState({selectedMedication: table[index]}, function() { console.log("setState medication", this.state) });
        this.setState({seeUpdateMedicationForm: true}); ///porneste form
    }

    doTheUpdate(newMedication){ //partial update data for tha patient
        let updatedMedication = this.state.selectedMedication;
        updatedMedication.name = newMedication.name;
        updatedMedication.dosage = newMedication.dosage;
        updatedMedication.sideeffects = newMedication.sideeffects;

        return API_MEDICATION.updateMedication(updatedMedication, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated patient with id: " + result);
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
            this.setState({seeUpdateMedicationForm: false}); ///reseteaza sa dispara formul
        });
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchMedications();
    }

    fetchMedications() {
        return API_MEDICATION.getMedications((result, status, err) => {
            console.log(result);
            if (result !== null && status === 200) {
                result.forEach(x => {
                    this.tableData.push({
                        medication_id: x.medication_id,
                        name: x.name,
                        dosage: x.dosage,
                        sideeffects: x.sideeffects,
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Medication: Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh() {
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={this.columns}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>
                {this.state.seeUpdateMedicationForm &&
                <Row>
                    <Col>
                        <Card body>
                            <UpdateMedicationForm update={this.doTheUpdate}/>
                        </Card>
                    </Col>
                </Row>
                }

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <MedicationForm registerMedication={this.refresh}>

                                </MedicationForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Medications;
