import { EventEmitter } from "events";
import { Client } from "@stomp/stompjs";

export default class WebSocketListener extends EventEmitter {
    constructor() {  //am eliminat username, passwod ca nu am
        super();
        this.client = new Client( {
            brokerURL: "ws://" + "@localhost:8080/api/websocket",
            onConnect: () => {
                this.client.subscribe("/activity/events",
                    message => {
                    this.emit("event",JSON.parse(message.body));
                    console.log(message);
                    })
            }
        });
        this.client.activate(); //start listening to web socket
    }
}